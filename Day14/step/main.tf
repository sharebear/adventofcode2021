variable "input_state" {}

variable "mappings" {}

locals {
  input_length = length(var.input_state) - 1
  output_parts = concat(
    [for i, v in slice(var.input_state, 0, local.input_length) : var.mappings["${v}${var.input_state[i + 1]}"]],
    [var.input_state[local.input_length]]
  )
}

output "output_state" {
  value = flatten(local.output_parts)
}