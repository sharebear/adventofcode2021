locals {
  input                = file("${path.module}/input.txt")
  input_as_string_list = compact(split("\n", local.input))
  input_state          = regexall("[A-Z]", local.input_as_string_list[0])
  mapping_rows         = slice(local.input_as_string_list, 1, length(local.input_as_string_list))
  mapping_tokens       = [for v in local.mapping_rows : split(" ", v)]
  mapping_keys         = [for v in local.mapping_tokens : v[0]]
  mapping_values       = [for v in local.mapping_tokens : [substr(v[0], 0, 1), v[2]]]
  mappings             = zipmap(local.mapping_keys, local.mapping_values)
}

module "step_1" {
  source = "./step"
  input_state  = local.input_state
  mappings = local.mappings
}

module "step_2" {
  source = "./step"
  input_state  = module.step_1.output_state
  mappings = local.mappings
}

module "step_3" {
  source = "./step"
  input_state  = module.step_2.output_state
  mappings = local.mappings
}

module "step_4" {
  source = "./step"
  input_state  = module.step_3.output_state
  mappings = local.mappings
}

module "step_5" {
  source = "./step"
  input_state  = module.step_4.output_state
  mappings = local.mappings
}

module "step_6" {
  source = "./step"
  input_state  = module.step_5.output_state
  mappings = local.mappings
}

module "step_7" {
  source = "./step"
  input_state  = module.step_6.output_state
  mappings = local.mappings
}

module "step_8" {
  source = "./step"
  input_state  = module.step_7.output_state
  mappings = local.mappings
}

module "step_9" {
  source = "./step"
  input_state  = module.step_8.output_state
  mappings = local.mappings
}

module "step_10" {
  source = "./step"
  input_state  = module.step_9.output_state
  mappings = local.mappings
}

locals {
  output_state = join("", module.step_10.output_state)
  letters                = regexall("[A-Z]", "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
  letter_counts          = [for v in local.letters : length(regexall(v, local.output_state))]
  max_letter_count       = max(local.letter_counts...)
  non_zero_letter_counts = [for v in local.letter_counts : v if v > 0]
  min_letter_count       = min(local.non_zero_letter_counts...)
  result                 = local.max_letter_count - local.min_letter_count
}

output "output_1" {
  value = local.result
}
