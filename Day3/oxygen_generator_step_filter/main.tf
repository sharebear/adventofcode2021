variable "input" {
  
}

variable "index" {
  type = number
}

output "result" {
  value = [for v in var.input : v if substr(v, var.index, 1) == (length([for s in var.input : 1 if substr(s, var.index, 1) == "1"]) >= length([for s in var.input : 1 if substr(s, var.index, 1) == "0"]) ? "1" : "0")]
}