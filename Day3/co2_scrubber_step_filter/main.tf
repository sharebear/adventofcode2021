variable "input" {
}

variable "index" {
  type = number
}

locals {
  one_frequency = length([for s in var.input : 1 if substr(s, var.index, 1) == "1"])
  zero_frequency = length([for s in var.input : 1 if substr(s, var.index, 1) == "0"])
}

output "one_frequency" {
  value = local.one_frequency
}

output "zero_frequency" {
  value = local.zero_frequency
}

output "result" {
  value = length(var.input) > 1 ? [for v in var.input : v if substr(v, var.index, 1) == (local.one_frequency < local.zero_frequency ? "1" : "0")] : var.input
}