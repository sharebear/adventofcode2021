locals {
  input                = file("${path.module}/input.txt")
  input_as_string_list = compact(split("\n", local.input))
  input_length         = length(local.input_as_string_list[0])
  one_bit_counts       = [for i in range(local.input_length) : sum([for s in local.input_as_string_list : 1 if substr(s, i, 1) == "1"])]
  zero_bit_counts      = [for i in range(local.input_length) : sum([for s in local.input_as_string_list : 1 if substr(s, i, 1) == "0"])]
  gamma_rate           = [for i in range(local.input_length) : local.one_bit_counts[i] > local.zero_bit_counts[i] ? 1 : 0]
  epsilon_rate         = [for i in range(local.input_length) : local.one_bit_counts[i] < local.zero_bit_counts[i] ? 1 : 0]
  gamma_as_decimal     = sum([for i, v in reverse(range(local.input_length)) : local.gamma_rate[v] * pow(2, i)])
  epsilon_as_decimal   = sum([for i, v in reverse(range(local.input_length)) : local.epsilon_rate[v] * pow(2, i)])
  power_consumption    = local.gamma_as_decimal * local.epsilon_as_decimal
}

output "result_1" {
  value = local.power_consumption
}

module "oxygen_generator_0" {
  source = "./oxygen_generator_step_filter"
  input  = local.input_as_string_list
  index  = 0
}

module "oxygen_generator_1" {
  source = "./oxygen_generator_step_filter"
  input  = module.oxygen_generator_0.result
  index  = 1
}

module "oxygen_generator_2" {
  source = "./oxygen_generator_step_filter"
  input  = module.oxygen_generator_1.result
  index  = 2
}

module "oxygen_generator_3" {
  source = "./oxygen_generator_step_filter"
  input  = module.oxygen_generator_2.result
  index  = 3
}

module "oxygen_generator_4" {
  source = "./oxygen_generator_step_filter"
  input  = module.oxygen_generator_3.result
  index  = 4
}
module "oxygen_generator_5" {
  count = local.input_length > 5 ? 1 : 0
  source = "./oxygen_generator_step_filter"
  input  = module.oxygen_generator_4.result
  index  = 5
}
module "oxygen_generator_6" {
  count = local.input_length > 5 ? 1 : 0
  source = "./oxygen_generator_step_filter"
  input  = module.oxygen_generator_5[0].result
  index  = 6
}
module "oxygen_generator_7" {
  count = local.input_length > 5 ? 1 : 0
  source = "./oxygen_generator_step_filter"
  input  = module.oxygen_generator_6[0].result
  index  = 7
}
module "oxygen_generator_8" {
  count = local.input_length > 5 ? 1 : 0
  source = "./oxygen_generator_step_filter"
  input  = module.oxygen_generator_7[0].result
  index  = 8
}
module "oxygen_generator_9" {
  count = local.input_length > 5 ? 1 : 0
  source = "./oxygen_generator_step_filter"
  input  = module.oxygen_generator_8[0].result
  index  = 9
}
module "oxygen_generator_10" {
  count = local.input_length > 5 ? 1 : 0
  source = "./oxygen_generator_step_filter"
  input  = module.oxygen_generator_9[0].result
  index  = 10
}
module "oxygen_generator_11" {
  count = local.input_length > 5 ? 1 : 0
  source = "./oxygen_generator_step_filter"
  input  = module.oxygen_generator_10[0].result
  index  = 11
}

locals {
  oxygen_generator_rating = local.input_length > 5 ? module.oxygen_generator_11[0].result[0] : module.oxygen_generator_4.result[0]
  ogr_decimal             = sum([for i, v in reverse(range(local.input_length)) : tonumber(substr(local.oxygen_generator_rating, v, 1)) * pow(2, i)])
}

module "co2_scrubber_step_0" {
  source = "./co2_scrubber_step_filter"
  input  = local.input_as_string_list
  index  = 0
}

module "co2_scrubber_step_1" {
  source = "./co2_scrubber_step_filter"
  input  = module.co2_scrubber_step_0.result
  index  = 1
}
module "co2_scrubber_step_2" {
  source = "./co2_scrubber_step_filter"
  input  = module.co2_scrubber_step_1.result
  index  = 2
}
module "co2_scrubber_step_3" {
  source = "./co2_scrubber_step_filter"
  input  = module.co2_scrubber_step_2.result
  index  = 3
}
module "co2_scrubber_step_4" {
  source = "./co2_scrubber_step_filter"
  input  = module.co2_scrubber_step_3.result
  index  = 4
}

module "co2_scrubber_step_5" {
  count = local.input_length > 5 ? 1 : 0
  source = "./co2_scrubber_step_filter"
  input  = module.co2_scrubber_step_4.result
  index  = 5
}

module "co2_scrubber_step_6" {
  count = local.input_length > 5 ? 1 : 0
  source = "./co2_scrubber_step_filter"
  input  = module.co2_scrubber_step_5[0].result
  index  = 6
}

module "co2_scrubber_step_7" {
  count = local.input_length > 5 ? 1 : 0
  source = "./co2_scrubber_step_filter"
  input  = module.co2_scrubber_step_6[0].result
  index  = 7
}

module "co2_scrubber_step_8" {
  count = local.input_length > 5 ? 1 : 0
  source = "./co2_scrubber_step_filter"
  input  = module.co2_scrubber_step_7[0].result
  index  = 8
}

module "co2_scrubber_step_9" {
  count = local.input_length > 5 ? 1 : 0
  source = "./co2_scrubber_step_filter"
  input  = module.co2_scrubber_step_8[0].result
  index  = 9
}

module "co2_scrubber_step_10" {
  count = local.input_length > 5 ? 1 : 0
  source = "./co2_scrubber_step_filter"
  input  = module.co2_scrubber_step_9[0].result
  index  = 10
}

module "co2_scrubber_step_11" {
  count = local.input_length > 5 ? 1 : 0
  source = "./co2_scrubber_step_filter"
  input  = module.co2_scrubber_step_10[0].result
  index  = 11
}

locals {
  co2_scrubber_rating = local.input_length > 5 ? module.co2_scrubber_step_11[0].result[0] : module.co2_scrubber_step_4.result[0]
  csr_decimal         = sum([for i, v in reverse(range(local.input_length)) : tonumber(substr(local.co2_scrubber_rating, v, 1)) * pow(2, i)])
  life_support_rating = local.csr_decimal * local.ogr_decimal
}

output "result_2" {
  value = local.life_support_rating
}
