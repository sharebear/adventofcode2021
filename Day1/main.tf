locals {
  input                = file("${path.module}/input.txt")
  input_as_string_list = compact(split("\n", local.input))
  input_as_number_list = [for v in local.input_as_string_list : tonumber(v)]
  increased_or_not = [for i, v in local.input_as_number_list :
    i == 0 ? "" : (v - local.input_as_number_list[i - 1] > 0 ? "increased" : "")
  ]
  output_1 = length(compact(local.increased_or_not))
}

output "output_1" {
  value = local.output_1
}

## Part 2

locals {
  window_start_values = slice(
    local.input_as_number_list,
    0,
    length(local.input_as_number_list) - 2
  )
  window_start_indexes = [for i, v in local.window_start_values : i]
  window_sums = [for i in local.window_start_indexes : sum([
    local.input_as_number_list[i],
    local.input_as_number_list[i + 1],
    local.input_as_number_list[i + 2],
  ])]
  window_increased_or_not = [for i, v in local.window_sums :
    i == 0 ? "" : (v - local.window_sums[i - 1] > 0 ? "increased" : "")
  ]
  output_2 = length(compact(local.window_increased_or_not))
}

output "output_2" {
  value = local.output_2
}
