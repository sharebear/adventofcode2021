locals {
  input                 = file("${path.module}/input.txt")
  input_as_string_list  = compact(split("\n", local.input))
  tokenized_lines       = [for line in local.input_as_string_list : split(" ", line)]
  output_values         = [for line in local.tokenized_lines : slice(line, 11, 15)]
  segment_counts        = [for line in local.output_values : [for segment in line : length(segment)]]
  identifyable_segments = [for line in local.segment_counts : [for segment in line : contains([2, 3, 4, 7], segment) ? 1 : 0]]
  identifyable_count    = sum(flatten(local.identifyable_segments))
}

output "output_1" {
  value = local.identifyable_count
}
