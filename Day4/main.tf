locals {
  input                   = file("${path.module}/input.txt")
  input_as_string_list    = compact(split("\n", local.input))
  number_of_boards        = (length(local.input_as_string_list) - 1) / 5
  board_start_lines       = [for v in range(local.number_of_boards) : 5 * v + 1]
  board_lines             = [for v in local.board_start_lines : slice(local.input_as_string_list, v, v + 5)]
  board_cells_strings     = [for v in local.board_lines : [for line in v : compact(split(" ", line))]]
  board_cells_numbers     = [for v in local.board_cells_strings : [for line in v : [for cell in line : tonumber(cell)]]]
  draws                   = [for token in split(",", local.input_as_string_list[0]) : tonumber(token)]
  individual_draw_results = [for draw in local.draws : [for board in local.board_cells_numbers : [for row in board : [for cell in row : cell == draw ? 1 : 0]]]]
  cumulative_draw_results = [for draw_idx, draw in local.draws : [for board_idx, board in local.board_cells_numbers : [for row_idx, row in board : [for cell_idx, cell in row : sum(
    [for d2 in slice(local.individual_draw_results, 0, draw_idx + 1) : d2[board_idx][row_idx][cell_idx]]
  )]]]]
  # Each board gets a 1 or 0 depending on wether it has a complete row or column
  draw_board_status = [for draw in local.cumulative_draw_results : [for board in draw : sum(concat(
    [for row in board : sum(row) == 5 ? 1 : 0],
    [for cell_idx in range(5) : sum([for row in board : row[cell_idx]]) == 5 ? 1 : 0]
  )) > 0 ? 1 : 0]]
  # Draw gets a 1 if there is a winning board after completion of the draw
  draw_status              = [for draw in local.draw_board_status : sum(draw) > 0 ? 1 : 0]
  winning_draw_index       = index(local.draw_status, 1)
  winning_board_index      = index(local.draw_board_status[local.winning_draw_index], 1)
  winning_unmarked_numbers = flatten([for row_idx, row in local.cumulative_draw_results[local.winning_draw_index][local.winning_board_index] : [for cell_idx, cell in row : local.board_cells_numbers[local.winning_board_index][row_idx][cell_idx] if cell == 0]])
  score                    = sum(local.winning_unmarked_numbers) * local.draws[local.winning_draw_index]
}

output "result_1" {
  value = local.score
}

locals {
  last_winning_draw_index = index(reverse(local.draw_status), 1)
  #FIXME Change index
  last_winning_board_index = index(local.draw_board_status[local.winning_draw_index], 1)
}
