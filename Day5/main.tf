locals {
  input                = file("${path.module}/input.txt")
  input_as_string_list = compact(split("\n", local.input))
  board_size           = 1000
  line_tokens          = [for line in local.input_as_string_list : split(" ", line)]
  line_only_digits     = [for line in local.line_tokens : concat(split(",", line[0]), split(",", line[2]))]
  lines_as_numbers     = [for line in local.line_only_digits : [for num in line : tonumber(num)]]
  lines_as_obj = [for line in local.lines_as_numbers : {
    start : { x : line[0], y : line[1] }
    end : { x : line[2], y : line[3] }
  }]

  lines_as_ranges = [for line in local.lines_as_obj: line.start.y == line.end.y || line.start.x == line.end.x ? {
    y_range = range(min(line.start.y, line.end.y), max(line.start.y, line.end.y) + 1)
    x_range = range(min(line.start.x, line.end.x), max(line.start.x, line.end.x) + 1)
  } : {
    y_range = []
    x_range = []
  }]

  end_state = [for y in range(local.board_size): [for x in range(local.board_size): sum([for r in local.lines_as_ranges: contains(r.x_range, x) && contains(r.y_range, y) ? 1 : 0 ])]]

  numbers_greater_than_1 = [for y in range(local.board_size) : [for x in range(local.board_size): local.end_state[y][x] > 1 ? 1 : 0]]
  total_greater_than_1 = sum(flatten(local.numbers_greater_than_1))
}

output "result_1" {
  value = local.total_greater_than_1
}
