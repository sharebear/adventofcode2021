locals {
  input                  = file("${path.module}/input.txt")
  input_as_string_list   = compact(split("\n", local.input))
  input_line_as_string   = local.input_as_string_list[0]
  input_as_strings       = split(",", local.input_line_as_string)
  input_as_numbers       = [for s in local.input_as_strings : tonumber(s)]
  lowest_number          = min(local.input_as_numbers...)
  highest_number         = max(local.input_as_numbers...)
  positions              = slice(concat(local.input_as_numbers, local.input_as_numbers), 0, local.highest_number + 1)
  distances_to_positions = [for i, v in local.positions : [for crab_pos in local.input_as_numbers : abs(crab_pos - i)]]
  cost_to_position       = [for v in local.distances_to_positions : sum(v)]
  minimal_fuel           = min(local.cost_to_position...)
}

output "result_1" {
  value = local.minimal_fuel
}

locals {
  correct_crab_cost_to_positions = [for v in local.distances_to_positions : [for crab_pos in v : crab_pos * (crab_pos + 1) / 2]]
  correct_cost_to_position       = [for v in local.correct_crab_cost_to_positions : sum(v)]
  correct_minimal_fuel           = min(local.correct_cost_to_position...)
}

output "result_2" {
  value = local.correct_minimal_fuel
}
