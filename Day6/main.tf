variable "day" {
  default = 1
  type    = number
}

locals {
  input                = file(var.day == 1 ? "${path.module}/input.txt" : "${path.module}/scratch/${var.day - 1}.txt")
  input_as_string_list = compact(split("\n", local.input))
  fish_strings         = split(",", local.input_as_string_list[0])
  fish                 = [for f in local.fish_strings : tonumber(f)]
  counts = var.day == 1 ? [
    length([for f in local.fish : f if f == 0]),
    length([for f in local.fish : f if f == 1]),
    length([for f in local.fish : f if f == 2]),
    length([for f in local.fish : f if f == 3]),
    length([for f in local.fish : f if f == 4]),
    length([for f in local.fish : f if f == 5]),
    length([for f in local.fish : f if f == 6]),
    length([for f in local.fish : f if f == 7]),
    length([for f in local.fish : f if f == 8]),
  ] : local.fish
  aged_counts = [
    local.counts[1],
    local.counts[2],
    local.counts[3],
    local.counts[4],
    local.counts[5],
    local.counts[6],
    local.counts[7] + local.counts[0],
    local.counts[8],
    local.counts[0],
  ]
  fish_count    = sum(flatten(local.aged_counts))
  result_string = join(",", local.aged_counts)
}

resource "local_file" "today" {
  filename = "${path.module}/scratch/${var.day}.txt"
  content  = local.result_string
}

output "result_1" {
  value = local.fish_count
}
