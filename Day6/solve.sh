#!/usr/bin/env bash

set -e

for number in {1..256}; do
  time terraform apply -auto-approve -var="day=${number}"
done
