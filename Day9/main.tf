locals {
  input                = file("${path.module}/input.txt")
  input_as_string_list = compact(split("\n", local.input))
  line_lengths         = length(local.input_as_string_list[0])
  line_count           = length(local.input_as_string_list)
  char_indexes         = range(local.line_lengths)
  heightmap            = [for line in local.input_as_string_list : [for char_idx in local.char_indexes : tonumber(substr(line, char_idx, 1))]]
  lower_than_above     = [for line_idx, line in local.heightmap : [for height_idx, height in line : line_idx == 0 ? 1 : height < local.heightmap[line_idx - 1][height_idx] ? 1 : 0]]
  lower_than_right     = [for line_idx, line in local.heightmap : [for height_idx, height in line : height_idx == local.line_lengths - 1 ? 1 : height < local.heightmap[line_idx][height_idx + 1] ? 1 : 0]]
  lower_than_below     = [for line_idx, line in local.heightmap : [for height_idx, height in line : line_idx == local.line_count - 1 ? 1 : height < local.heightmap[line_idx + 1][height_idx] ? 1 : 0]]
  lower_than_left      = [for line_idx, line in local.heightmap : [for height_idx, height in line : height_idx == 0 ? 1 : height < local.heightmap[line_idx][height_idx - 1] ? 1 : 0]]
  risk_levels          = [for line_idx, line in local.heightmap : [for height_idx, height in line : (local.lower_than_above[line_idx][height_idx] + local.lower_than_right[line_idx][height_idx] + local.lower_than_below[line_idx][height_idx] + local.lower_than_left[line_idx][height_idx]) == 4 ? height + 1 : 0]]
  sum_risk_levels      = sum(flatten(local.risk_levels))
}

output "output_1" {
  value = local.sum_risk_levels
}
