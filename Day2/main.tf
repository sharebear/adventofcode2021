locals {
  input                = file("${path.module}/input.txt")
  input_as_string_list = compact(split("\n", local.input))
  lines_as_tuples      = [for l in local.input_as_string_list : split(" ", l)]
  down_values          = [for t in local.lines_as_tuples : tonumber(t[1]) if t[0] == "down"]
  up_values            = [for t in local.lines_as_tuples : tonumber(t[1]) if t[0] == "up"]
  forward_values       = [for t in local.lines_as_tuples : tonumber(t[1]) if t[0] == "forward"]
  depth                = sum(local.down_values) - sum(local.up_values)
  position             = sum(local.forward_values)
  result_1             = local.depth * local.position
}

output "result_1" {
  value = local.result_1
}

locals {
  input_as_objects = [for l in local.lines_as_tuples : { action : l[0], effect : tonumber(l[1]) }]
  aim_sources      = [for i, v in local.input_as_objects : slice(local.input_as_objects, 0, i + 1)]
  aim_values       = [for v in local.aim_sources : [for o in v : o.action == "down" ? o.effect : (o.action == "up" ? -1 * o.effect : 0)]]
  aim_sums         = [for v in local.aim_values : sum(v)]
  position_changes = [for v in local.input_as_objects : v.action == "forward" ? v.effect : 0]
  final_position   = sum(local.position_changes)
  depth_changes    = [for i, v in local.input_as_objects : v.action == "forward" ? v.effect * local.aim_sums[i] : 0]
  final_depth      = sum(local.depth_changes)
  result_2 = local.final_position * local.final_depth
}


output "result_2" {
  value = local.result_2
}
# 
