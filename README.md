# 🎄 Advent of Code 2021 in Terraform 🎄

After reading a discussion on the HashiCorp Ambassador Slack of how to one
complex datastructure to another in order to maintain a nice module API I got
wondering if the Advent of Code challenges were solvable with Terraform.

This repository contains the results of this thought experiment.

## 🚨 Warning

This code is neither pretty, documented nor a good example of how I use and
write Terraform normally. I tend to stop coding immediately once I have a
working solution so that this doesn't consume every waking hour.

I may also lose interest quickly once the tasks get harder...

## 👩‍💻 Running the code

The code is split into a different terraform root module per day. Running the
code should be as simple as navigating to the example you want to run and
running terraform refresh or terraform apply.

```bash
cd Day1
terraform init
terraform refresh
```

Which should result in the following output where `result_1` represents the
answer for the first star and `result_2` the answer for the second star.

```
╷
│ Warning: Empty or non-existent state
│ 
│ There are currently no resources tracked in the state, so there is nothing to refresh.
╵

Outputs:

result_1 = 1139
result_2 = 1103
```

## :100: Status

| Challenge | Example Part 1 | Real Part 1 | Example Part 2 | Real Part 2 |
| ---                             | ---    | ---    | ---    | ---    |
| Day  1: Sonar Sweep             | :star: | :star: | :star: | :star: |
| Day  2: Dive!                   | :star: | :star: | :star: | :star: |
| Day  3: Binary Diagnostic       | :star: | :star: | :star: | :star: |
| Day  4: Giant Squid             | :star: | :star: | :star: |        |
| Day  5: Hydrothermal Venture    |        |        |        |        |
| Day  6: Lanternfish             | :star: | :star: | :star: | :star: |
| Day  7: The Treachery of Whales | :star: | :star: | :star: | :star: |
| Day  8: Seven Segment Search    | :star: | :star: |        |        |
| Day  9: Smoke Basin             | :star: | :star: |        |        |
| Day 10:                         |        |        |        |        |
| Day 11:                         |        |        |        |        |
| Day 12:                         |        |        |        |        |
| Day 13:                         |        |        |        |        |
| Day 14: Extended Polymerization | :star: | :star: |        |        |
